#!/bin/zsh

VERSION="0.1.0"
echo "· Wi-Fi connection script v$VERSION - http://code.seawolfsanctuary.com/wi-fi_cli ·\n"

IFACE=""
ESSID=""
KEY=""

parse_args() {
  echo "» Parsing options: $*"
  while getopts ":hi:e:k:" opt; do
    case $opt in
      h)
        echo "  -h  Shows this help (also: --help)"
        echo "  -i  the wireless interface for the connection   (required)"
        echo "      -i wlan0"
        echo "  -e  the network ESSID (name) for the connection   (required)"
        echo "      -e HomeRouter-E12BD0"
        echo "  -k  the encryption key (password) for the network   (optional)"
        echo "      -k A1B2C3D4E5F6G7H8"
        exit 1
        ;;
      i)
        IFACE=$OPTARG
        ;;
      e)
        ESSID=$OPTARG
        ;;
      k)
        KEY=$OPTARG
        ;;
      \?)
        echo "  ! Invalid option: -$OPTARG" >&2
        exit 1
        ;;
      :)
        echo "  ! The -$OPTARG option requires an argument." >&2
        exit 1
        ;;
    esac
  done
}

check_args() {
  echo "  » interface: $IFACE"
  if [[ -z "$IFACE" ]]; then
    echo "  ! You must supply a wireless interface for the connection. This is usually wlan0."
    exit 1
  fi

  echo "  » network  : $ESSID"
  if [[ -z "$ESSID" ]]; then
    echo "  ! You must supply a network ESSID (name) for the connection. This is the network to which you want to connect."
    exit 1
  fi

  echo "  » enc. key : $KEY"
}

prepare_iface() {
  echo "\n» Preparing interface: $IFACE"
  ifconfig $IFACE down
  ifconfig $IFACE up
}

scan() {
  echo "\n» Scanning for networks..."
  iwlist $IFACE scan | grep -E -o "ESSID:(.*)" | sed 's/ESSID:/  » /g'
}

initialise_network() {
  echo "\n» Connecting to network: $ESSID"
  CMD="iwconfig $IFACE essid $ESSID"
  if [[ ! -z "$KEY" ]]; then
    CMD="$CMD key s:$KEY"
  fi
  $CMD
}

obtain_dhcp() {
  echo "\n» Releasing previous DHCP lease and IP address..."
  dhclient -r $IFACE
  echo "\n» Obtaining new DHCP lease and IP address..."
  dhclient    $IFACE
}

if [[ "$*" == "--help" ]]; then
  parse_args "-h"
else
  parse_args $* && check_args && prepare_iface && initialise_network && obtain_dhcp
fi
